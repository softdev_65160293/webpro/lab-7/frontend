import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import axios from 'axios'
import http from '@/services/http'
import { useLoadingStore } from './loading'
import temperatureService from '@/services/temperature'

export const useTemperatureStore = defineStore('temperature', () => {
  const valid = ref(false)
  const celsius = ref(0)
  const result = ref(0)

  const loadingStore = useLoadingStore()

  async function callConvert() {
    loadingStore.doLoad()

    // result.value = convert(celsius.value)
    // GET FUNTION PARAM
    // try {
    //   console.log(`http://localhost:3000/temperature/convert/${celsius.value}`)
    //   const respond = await axios.get(`http://localhost:3000/temperature/convert/${celsius.value}`)
    //   const converResult = respond.data as ReturnData
    //   console.log(converResult)
    //   result.value = converResult.fahrenheit
    // } catch (e) {
    //   console.log(e)
    // }

    // POST FUNTION
    try {
      result.value = await temperatureService.convert(celsius.value)
    } catch (e) {
      console.log(e)
    }
    loadingStore.finish()

    // .then((respond) => {
    //   const converResult = respond.data as ReturnData
    //   console.log(converResult)
    //   result.value = converResult.fahrenheit
    // })
    // .catch((err) => {
    //   console.log(err)
    // })
  }

  return { valid, result, celsius, callConvert }
})
