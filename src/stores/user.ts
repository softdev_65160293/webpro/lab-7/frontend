import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'
import { useLoadingStore } from './loading'
import userService from '@/services/user'

export const useUserStore = defineStore('user', () => {
  const loadingStore = useLoadingStore()
  const users = ref<User[]>([])

  const initialUser: User = {
    email: '',
    password: '',
    fullName: '',
    gender: 'male',
    roles: ['user']
  }
  const editedUser = ref<User>(JSON.parse(JSON.stringify(initialUser)))

  async function getUsers() {
    loadingStore.doLoad()
    const res = await userService.getUsers()
    users.value = res.data
    loadingStore.finish()
  }

  async function getUser(id: number) {
    loadingStore.doLoad()
    const res = await userService.getUser(id)
    editedUser.value = res.data
    loadingStore.finish()
  }

  async function saveUser() {
    loadingStore.doLoad()
    const user = editedUser.value
    if (!user.id) {
      // add new
      const res = await userService.addNew(user)
    } else {
      // update
      const res = await userService.update(user)
    }
    await getUsers()
    loadingStore.finish()
  }

  async function deleteUser() {
    loadingStore.doLoad()
    const user = editedUser.value
    const res = await userService.remove(user)
    await getUsers()
    loadingStore.finish()
  }

  function clearForm() {
    editedUser.value = JSON.parse(JSON.stringify(initialUser))
  }

  return { users, deleteUser, saveUser, getUsers, getUser, editedUser, initialUser, clearForm }
})
