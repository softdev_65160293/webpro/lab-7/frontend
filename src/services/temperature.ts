import http from './http'

type ReturnData = {
  celsius: number
  fahrenheit: number
}

async function convert(celsius: number): Promise<number> {
  console.log(`/temperature/convert/${celsius}`)
  const respond = await http.post(`/temperature/convert`, {
    celsius: celsius
  })
  const converResult = respond.data as ReturnData
  return converResult.fahrenheit
}

export default { convert }
